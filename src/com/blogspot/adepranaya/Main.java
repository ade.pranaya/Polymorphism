/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.adepranaya;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author depran
 */
public class Main {
    public static void main(String[] args) {
        List<Orang> oList = new ArrayList<>();
        Orang tc = new TukangCukur("joko", 12);
        Orang db = new Dokter("budi", 33);
        Orang a = new Aktor("lina", 25);
        
        
        oList.add(tc);
        oList.add(db);
        oList.add(a);
        
        for (Orang orang : oList) {
            orang.cut();
        }
    }
    
}
