/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.adepranaya;

/**
 *
 * @author depran
 */
public class Dokter extends Orang{

    public Dokter(String nama, int umur) {
        super(nama, umur);
    }

    @Override
    public void cut() {
        System.out.println(getNama()+" akan memulai membuat irisan di tubuh pasiennya");
    }
    
}
