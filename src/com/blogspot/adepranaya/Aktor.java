/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogspot.adepranaya;

/**
 *
 * @author depran
 */
public class Aktor extends Orang{

    public Aktor(String nama, int umur) {
        super(nama, umur);
    }

    @Override
    public void cut() {
        System.out.println(getNama()+" akan menghentikan aktingnya, menunggu perintah berikutnya");
    }
    
}
